#==========================================================================
#  Dirichlet process for local calibration
#  
# Author : Damien Juery
# Date : April 16, 2012
#
# Aim: this function uses Dirichlet process (Neal 2000) to detect nearest neighbour samples 
#		X is a (n*p) matrix containing all the possible calibration samples (each column is a predictor (wavelength), each row is a sample)
#		N is the number of iterations to perform to converge to the solution. By default, N = 200.
#		tau is the tunning parameter. By default, tau = 0.05
#
# Reference:
#	Neal, R.M. (2000). Markov Chain Sampling Methods for Dirichlet Process Mixture Models. J. Computational and Graphical Statistics 9(2), 249-265.
#
#==========================================================================

DP <- function (X, N = 200, tau = 0.05, spline=TRUE) 
{

	
	## Spline regression for reducing the dimension
		# spline regression package
	library(splines)  
		# internal breakpoints that define the spline
	ki <- seq(5, ncol(X) - 5, length = 100)  
		# degree of polynomial
	deg <- 2 
		# L: total degrees of freedom (total number of knots)
	L <- length(ki) + deg + 1  
		# defines the splines basis
	test.bs <- bs(1:ncol(X), knots = ki, degree = deg)
		# initialisation  
	test.lm <- list(); beta <- list()  
	for (i in 1:nrow(X)) {
		test.lm[[i]] <- lm(as.numeric(X[i,]) ~ test.bs)
			# spline regression coefficients
		beta[[i]] <- test.lm[[i]]$coefficients[1:L]  
	}

	## Fixing hyperparameters for the Dirichlet Process Mixture (DPM) model
	mu <- Reduce("+",beta)/nrow(X)
	tau_G0 <- sd(unlist(lapply(beta,function(x){x[2:L]})))

	## Inference for the Dirichlet Process Mixture (DPM) model
	library(mvtnorm)  # multivariate gaussian package
	nbPred <- nrow(X)  # number of predictors
	c <- rep(1, nbPred)
	psi <- list(); values <- list()
	psi[[1]] <- mu
	alpha_0 <- 1
	
	## N : number of iterations to produce into the Gibbs sampler (MCMC)
	

	## LOOP on iterations
	for (n in 1:N) {
	  
		## LOOP on curves clustering assignments
		for (i in 1:nbPred) {  
			cc <- c[-i]
			tai_k <- unique(cc)
			prob_old_val <- apply(as.matrix(tai_k), 1, function(y) {
                                                 sum(cc==y) * dmvnorm(beta[[i]], mean = psi[[y]], sigma = diag(tau^2, L, L))
                                                           })
			prob_new_val <- alpha_0 * dmvnorm(beta[[i]], mean = mu, sigma = diag(tau^2 + tau_G0^2, L, L))

				# numerical normalisation constant
			cte <- 1 / (sum(prob_old_val) + prob_new_val)  
			prob_old_val <- prob_old_val * cte
			prob_new_val <- prob_new_val * cte
				
				# cluster assignment
			c[i] <- sample(x = c(tai_k, max(tai_k) + 1), size = 1, prob = c(prob_old_val, prob_new_val))  

				# if the cluster assignment is a new one
			if(c[i] %in% tai_k == FALSE) {  
			sigma <- sqrt(1 / (1 / tau^2 + 1 / tau_G0^2))
			m_i <- sigma^2 * (beta[[i]] / tau^2 + mu / tau_G0^2)
			psi[[c[i]]] <- as.vector(rmvnorm(1, mean = m_i, sigma = diag(sigma^2, L, L)))
			}
		}
	
		# in order to produce no gaps between cluster indexes
		c <- as.numeric(as.factor(c))  
		psi <- psi[as.numeric(levels(as.factor(c)))]
	
		# LOOP on cluster centers
		for (k in 1:length(psi)) { 
			sigma_k <- sqrt(1 / (sum(c == k) / tau^2 + 1 / tau_G0^2))
			moy_k <- sigma_k^2 * (Reduce("+", beta[which(c == k)]) / tau^2 + mu / tau_G0^2)
			psi[[k]] <- as.vector(rmvnorm(1, mean = moy_k, sigma = diag(sigma_k^2, L, L)))
		}
	
		# Hyperprior parameters for Gamma prior
		a <- 0.01 ; b <- 0.01  
		eta <- rbeta(1, alpha_0 + 1, nbPred)
		poids <- (a + length(unique(c)) - 1) / (a + length(unique(c)) - 1 + nbPred * (b - log(eta)))
		alpha_0 <- sample(c(rgamma(1, shape = a + length(unique(c)), rate = b - log(eta)), rgamma(1, shape = a + length(unique(c)) - 1, rate = b - log(eta))), 1, prob = c(poids, 1 - poids))

		# saving iteration number n
		values[[n]] <- list(c = c, psi = psi, alpha_0 = alpha_0, tau = tau)  
		cat(paste("Iteration :", n, "\n"))
		cat(substr(date(), 12, 19),"\n")
		cat(table(c),"\n")

	## assigning "values" to global environment
	assign("values", values, envir = .GlobalEnv)  
	}

	## Dahl's (2006) criterion for choosing "best" clustering among iterations
		# selecting iterations from MCMC output
	start <- round(N/3); N <- N; step <- N/(N/6)  
	
		# selected iterations
	vec <- seq(start, N, by = step)  
		# number of selected iterations
	M <- length(vec)  

	# initialisation
	delta <- list()  
	pi_chapeau <- matrix(0, nbPred, nbPred)
	for (s in 1:length(vec)) {
		delta[[s]] <- apply(as.matrix(1:nbPred), 1, function(z) {
			apply(as.matrix(1:nbPred), 1, function(y) {
			as.integer(values[[vec[s]]]$c[z] == values[[vec[s]]]$c[y])
			})
		})
	show(s)
	pi_chapeau <- pi_chapeau + delta[[s]]
	}
	pi_chapeau <- 1 / length(vec) * pi_chapeau
	colnames(pi_chapeau) <- rownames(X)
	rownames(pi_chapeau) <- colnames(pi_chapeau)
	score <- apply(as.matrix(1:length(vec)), 1, function(y) {
		sum((delta[[y]] - pi_chapeau)^2)
	})
	
	# chosen clustering by Dahl's (2006) criterion
	config <- values[[vec[which.min(score)]]]$c
	names(config) <- rownames(X)
	return(list(config = config, pi.chapeau = pi_chapeau))
	

}
