#==========================================================================
#  Local calibration : neigbour selection based on a distance measure
#  Possibility to weight local samples if specified
#
#
# 	Author : Aurelie Thebault
# 	Date : March 26, 2012
#
#
#  Aim: this function detects nearest neighbour samples based on a metric distance metrics (R2 or H).
#      it creates a new calibration sample which can be weighted or not according to the distance
#      - z is a vector of p predictors (wavelengths) 
#      - X is a (n*p) matrix containing all the possible calibration samples (each column is a predictor (wavelength), each row is a sample)
#      - ref is a (n*v) matrix of predicted values  with samples in rows and variables reference values in columns
#      - method is the distance used to find neighbours. Can be	
#				- the correlation coefficient (r2, default), 
#				- or the Mahalanobis distance (H) 
#							
#      - Selection of neighbours can be done either by 
#            - giving a number of neigbours (Nneigh)
#            or 
#            - precising the limit to reach (in percentage of the lowest distance) to be considered as a neighbour. 
#
#	   - Nmin is the minimum number of neighbouring samples. By default, Nmim = 40.	
#			
#	Returns a list of files:
#       - Xcal : spectra of the calibration samples for the target sample 'spe'
#       - Ycal : reference values of the calibration samples for the target sample 'spe'
#       - weight : weight of the calibration samples, based on the distance to the target sample 'spe'
#       - Ndlim : number of calibration samples having distance to the target sample higher or equal to dlim
#     
#==========================================================================

localCal <- function(z, X, Y, method=c("r2", "H"), dlim , Nmin = 40, Nneigh, weight.cal=FALSE)
{

####################################### check the inputs
## -----------------------------------------------------

    Y <- as.matrix(Y)
    X <- as.matrix(X)
    

    if (length(z) != dim(X)[2]) {
        stop("the two files 'spe' and 'X' must have the same number of predictors")
    }  

    if (nrow(X) != nrow(Y)) {
        cat("WARNING: The file of spectras used to calibrate the prediction do not have the same number of samples of the file of reference values. Only common lines will be taken into account for the local calibration of the reference value matrix.","\n")
        cat("\n")
    }
 

    #function to check integers
    if (missing(Nmin))   {       
        stop("A minimum number of neighbour samples must be given")
    }
   
    if (missing(Nmin) == FALSE)   {  
        is.wholenumber <- function(x, tol = .Machine$double.eps^0.5)
                                    abs(x - round(x)) < tol  
        if (is.wholenumber(Nmin)==FALSE) {
            stop("Nmin must be an integer.")
        }
        if (Nmin<30)    {
            stop("the minimum number of neighbouring samples (Nmin) must be 30 or more.")
        }
        if (Nmin>dim(X)[1])     {
            stop("the minimum number of neighbouring samples cannot be higher than the total number of available calibration samples")
        }
    } 
     
    if (missing(Nneigh)== FALSE)   {       
        is.wholenumber <- function(x, tol = .Machine$double.eps^0.5)
                                    abs(x - round(x)) < tol  
        if (is.wholenumber(Nneigh)==FALSE) stop("Nneigh must be an integer.")
        if (Nneigh<30)  {
            stop("the number of neighbouring samples (Nneigh) must be 30 or more.")
        }
        if (Nneigh<Nmin) {
            stop("the number of neighbouring samples (Nneigh) can not be lower than the minimum number of neighbouring samples(Nmin).")
        }
        if (Nneigh>dim(X)[1])   {
            stop("the number of neighbouring samples cannot be higher than the total number of available calibration samples")
        }
    } 
  
    if (missing(dlim)== FALSE)  {     
        if (dlim>1) stop("dlim must be between 0 and 1.")
        if (dlim<0) stop("dlim must be between 0 and 1.")
    }
  
    if (missing(dlim) &  missing(Nneigh))  {        
        stop("A cutoff has to be precised, either 'dlim' or 'Nneigh'")
    }

##########################################################################


#############################  Creation of additional variables and files
## ----------------------------------------------------------------------

    ## Variables and matrices

    nbPred <- ncol(X)     # nbPred: number of predictors (wavelengths)
    nbVar <- ncol(Y)    # nbVar: number of variables to predict

    # merge matrix: only common rows from X and ref are included
    calSet <- merge(Y,X, by.x=0, by.y=0, all=FALSE) 
    rownames(calSet) <- calSet[,1]
    calSet <- calSet[,-1] 
    Xtmp <- calSet[,-c(1:nbVar)]
    rownames(Xtmp) <- rownames(calSet)
    Ytmp <- as.matrix(calSet[,c(1:nbVar)])
    rownames(Ytmp) <- rownames(calSet)
      
##################################################################

    #1 - Calculate distance between the sample to predict and each sample of the calibration set - Define neighbours
  
    if (method=="r2") {
       
        d <- apply(Xtmp, 1, cor,  z)
        
        # d2 : correlation coefficient
        d2 <- d^2
        
        # Selection of nearest neighbours
        if (missing(Nneigh) == FALSE)  {
            calDist <- sort(d2, decreasing = TRUE)[1: Nneigh]
            Ndlim <- length(calDist)
        }
        
        if (missing(dlim) == FALSE)  {
            calDist <- d2[d2 >= dlim * max(d2)]
            Ndlim <- length(calDist)
            if (length(calDist) < Nmin) {
                calDist <- sort(d2, decreasing = TRUE)[1: Nmin]
            }
        }
        
        # w : weight of all samples according to their distance to the target
        a <- 1/(1-d2)
        b <- rbind(a, rep(100, length(a)))
        w <- apply(b, 2, min)
    }

    if (method=="H") {
        # PCA on spectral data
        library(vegan)
        mix <- rbind(z, Xtmp)
        sp.pca <- prcomp(mix)
        ev <- sp.pca$sdev^2
        pc <- ev/sum(ev)
        # Mahalanobis distance calculated on 8 principal components
        sco <-sp.pca$x[,1:8]
            ## Calcul of Mahalanobis distance H for each sample
        d <- mahalanobis(sco[-1,], sco[1,], cov(sco[-1,]))
        dn <- d/max(d) # dn ranges between 0 (nearest neighbour) and 1 (furthest neighbour)

    
        # Selection of nearest neighbours
        if (missing(Nneigh) == FALSE)  {
            calDist <- sort(dn, decreasing = FALSE)[1:Nneigh]
            Ndlim <- length(calDist)
        }
        
        if (missing(dlim) == FALSE)  {
            # (1-dn) ranges from 0 (furthest neighbour) to 1 (nearest neighbour)
            calDist <- dn[(1-dn) >= dlim*(1-min(dn))]
            Ndlim <- length(calDist)
            if (length(calDist) < Nmin) {
                calDist <- sort(dn, decreasing = FALSE)[1: Nmin]
            }
        }
            
        # if (d2 > 1) d2 == 1, else d2 = d2 (see Naes and Isaksson 1992)
        d2inf1 <- apply(rbind(dn, rep(1, length(dn))), 2, min)     
        
        # w : weight of all samples according to their distance to the target
        w <- (1-d2inf1^3)^3
    }
    
    
    # 2 - Find calibration samples according to their distance values 
    tmp <- merge(calDist, calSet, by.x = 0, by.y =0)
    rownames(tmp) <- tmp[,1]
    colnames(tmp)[2] <- c("dist")
    tmp <- tmp [,-1]
    XCal <- tmp[,-c(1:((nbVar)+1))]  
    rownames(XCal) <- rownames(tmp)
    YCal <- as.matrix(tmp[,2:(nbVar+1)])
    rownames(YCal) <- rownames(tmp)
  
    # 3 - Define weights for calibration samples 
    wCaltmp <- merge(w, tmp, by.x=0, by.y=0)
    rownames(wCaltmp) <- wCaltmp[,1]
    colnames(wCaltmp)[2] <- c("weight")
    wCal <- wCaltmp [,2]
    names(wCal) <- rownames(wCaltmp)
    
        # Normalisation of the sample weights
    wCal <- wCal/sum(wCal) 
    
    
    # 4 - Calculate the new weigted matrices
    if (isTRUE(weight.cal)) {
        # 4a- Weighted Calibration matrix
        m <- t(XCal) %*% wCal
        XCal <- sweep(XCal, 2, m)
        # 4b- Weighted matrix of the Reference values
        my <- t(YCal) %*% wCal
        YCal <- as.matrix(sweep(YCal, 2, my))
    }
           
                             
    return(list(Xcal=XCal, Ycal = YCal, weight = wCal, Ndlim = Ndlim))
  
}
