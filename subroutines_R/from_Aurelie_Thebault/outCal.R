#==========================================================================
#  Function of calibration outliers removal based on T2 and mahalanobis distance
#
# Author : Aurelie Thebault
# Date : May 04, 2012
# 
# Required packages : none
#
# Aim: this function detecht calibration outliers based on the values of their residuals and the Mahalanobis distance (H) on a selected number of components (k). It removes the samples having residuals > 2.5 and Mahalanobis distance > H, which are considered as outliers
# 
#       - X is an unstransformed raw matrix containing wavelengths in columns and samples in rows
#       - pls is the model resuling from mvr() function of pls package
#       - k is the number of component PLS to take into account to extract outliers
#       - H is the limit Mahalanobis distance for outliers selection
#
#       Returns a list containing the new set(X without outliers) and the set of outliers - Directly in the form acceptable for mvr function
#
#==========================================================================

outCal <- function(X, pls, k, H)   {
	
	X <- as.matrix(X)
	
    # Outliers selected on the basis of residuals values
    res <- scale(residuals(pls)[,,k])
	# Merging with X matrix
	Xres <- merge(res, X, by.x=0, by.y = 0)
	colnames(Xres)[2] <- "res"
	rownames(Xres) <- Xres[,1]
    
    # if all residuals are below 2.5 => no outlier
    if (max(abs(Xres$res)) <= 2.5) {  
        t2cal <- X
        t2out <- NULL
    }
  
    # if at least 1 residual is higher than 2.5 
    if (max(abs(Xres$res)) > 2.5) {
    	t2cal <- Xres[abs(Xres$res) <= 2.5, ]
		rownames(t2cal) <- t2cal[,1]    	
        t2out <-  Xres[abs(Xres$res) > 2.5,]
        rownames(t2out) <- t2out[,1]
                    
    }
    
	
	# GH outliers - selected on the basis of the Mahalanobis distance
	sco <- scores(pls)[,1:k]
	mahal <- mahalanobis(sco, mean(sco), cov(sco))
	GH <- scale(mahal)
	cat("The scaled Mahalanobis distance vary from",round(sort(GH)[1],3),"to",round(sort(GH)[length(GH)],3),"\n") 
   
   	# merging with X matrix
   	XGH <- merge(GH, X, by.x=0, by.y = 0)
   	colnames(XGH)[2] <- "GH"
   	rownames(XGH) <- XGH[,1]
   	
    # if there is no outliers according to H
    if (H > max(XGH$GH)) { 
        GHcal <- X
        GHout <- NULL
    }
    
    # if there is at least 1 outlier according to H
    if(H<= max(XGH$GH)) { 
        GHcal <- XGH[XGH$GH <= H,]
        rownames(GHcal) <- GHcal[,1]
        GHout <- XGH[XGH$GH > H,]
        rownames(GHout) <- GHout[,1]
    }
    
    # Merging of outliers
    calOut <- rbind(t2out[,-c(1:2)],GHout[, -c(1:2)])
	cal <-merge(t2cal[,-c(1:2)],GHcal[,-c(1:2)], by.x=0, by.y = 0)
	rownames(cal) <- cal[,1]
	  
	  
    # Removing outliers from X
    Xout <- calOut
    Xcal <- X[cal[,1],]
    
    cat("Number of calibration outliers removed :", nrow(Xout),"\n" )


    # Plotting
    x11(title="Outlier detection")
    par(mfrow = c(1,2))
    
    plot(Xres$res, main = "Data residuals", ylab= "residuals")
    text(Xres$res, rownames(Xres), pos = 1, cex =0.6)
    abline(h = 2.5, col="red")
    abline(h = (-2.5), col="red")
    
    plot(XGH$GH, main = "Mahalanobis distance", 
                ylab= "Scaled Mahalanobis distance")
    text(XGH$GH, rownames(XGH), pos = 1, cex =0.6)
    abline(h = H, col="red")
   
    
    return(list(set=Xcal, out=Xout))
    
}
