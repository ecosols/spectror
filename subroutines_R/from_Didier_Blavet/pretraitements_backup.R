# 2.  Pretraitement des spectres (et graphes spectres pretraites) ----

palette(brewer.pal(12,"Paired")) # Pour avoir de belles couleurs !





#~~~ 2.1 fonctions necessitant package prospectr ----
# Spectres bruts ----
pretrait1 <- function(spec_init = Xbrut , spec_trans = Xm) 
{ 
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom, spec_init,inherits=TRUE)  
  wavelength <<- wavelength_brut 
 if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_init[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="Absorbance")
  mtext(paste(nom_pretrait,spec_trans_nom))
 }
}

# Moyenne mobile ----
pretrait2 <- function(spec_init = Xbrut , spec_trans =Xm) 
{ 
  spec_trans_nom <- deparse(substitute(spec_trans))
  # on applique la transformation et on transforme en cas de besoin 
  # spec_trans en data.frame 
    assign(spec_trans_nom,as.data.frame(movav(spec_init,w=10)) , inherits=TRUE)
    wavelength <<- as.numeric(colnames(spec_trans))
    if(option_graph_pretrait =="TRUE") {
    matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
            xlab="Longueur d'onde [nm]",ylab="Absorbance")
    mtext(nom_pretrait)
    }
}

# Centrage reduction ----
pretrait3 <- function(spec_init = Xbrut , spec_trans =Xm) 
{
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom,  standardNormalVariate(spec_init) , inherits=TRUE)
  # au lieu de wavelength[1:(dim(spec_trans)[2])] 
  # on utilise as.numeric(colnames(spec_trans))
  wavelength <<- as.numeric(colnames(spec_trans))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="Absorbance")
  mtext(nom_pretrait)
  }
}  

# Detrend ----
# ici il faut convertir Xm en dataframe sinon blocage plus loin
pretrait4 <- function(spec_init = Xbrut , spec_trans =Xm) 
{
  spec_trans_nom <- deparse(substitute(spec_trans))
  # on applique la transformation et on transforme en cas de besoin 
  # spec_trans en data.frame 
  assign(spec_trans_nom,as.data.frame(detrend(spec_init,wavelength_brut)), 
         inherits=TRUE)
    wavelength <<- as.numeric(colnames(spec_trans))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="Absorbance")
  mtext(nom_pretrait)
  }
}  
# dev.new() # ok
# pretrait4() # ok




# Continuum removal ----
# non generique a ce niveau il faut supprimer la premiere et la derniere 
# colonne et pour le je jeu de donnees Togo la colonne 324
# qui posent pb car valeurs nulles apres ce pretraitement
pretrait5 <- function(spec_init = Xbrut , spec_trans =Xm)
{
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom, 
  as.data.frame(continuumRemoval(spec_init,wavelength_brut,type="A")),
  inherits=TRUE)
  # on transforme spec_trans en data.frame
  # on va essayer d'enlever la première ET LA DERNIERE colonne de 
  # longueur d'onde qui ne contient plus que des zeros ce qui pose probleme 
  # pour la fonction ken.sto plus loin
  
  
  # identifie les longueurs d'onde avec uniquement des zeros
  # toto <- apply(as.matrix(Xm),MARGIN=2,FUN=sd) #  calcule les donnees 
                                                # avec que des zero
  # toto[toto==0]  localise les longueurs d'onde problemenatiques 
  
  
  ################# debut version avec longueur d'onde colonne 324 supprimee (pour NIRS)
  #  on avait tente aussi d'enlever la colonne 324 pour NIRS mais elle ne semble 
  #  pas si genante que cela car elle ne contient pas que des zeros
  # if (type_spectres == "NIRS") {
  # 
  #     assign(spec_trans_nom, 
  #     spec_trans[,c(-1,-324,-ncol(spec_trans))], inherits=TRUE)
  # 
  #     wavelength <<- as.numeric(colnames(spec_trans[,c(-1,-324,
  #                          -ncol(spec_trans))])) # on met aussi spectrans[,-1]
  #                               # ici car spec_trans reste à 700 variables
  #   # car manifestement la commande suivante ne change pas spec_trans 
  #   # dans la fonction  assign(spec_trans_nom,   spec_trans[,-1], inherits=TRUE)
  #   # ci-dessous on met aussi t(spec_trans[,-1]) (puis aussi derniere colonne) 
  #   # pour voir  
  # 
  #   if(option_graph_pretrait =="TRUE") {
  #   matplot(wavelength,t(spec_trans[,c(-1,-324,-ncol(spec_trans))]),
  #         type="l",col=c(1:12),lty=1,
  #         xlab="Longueur d'onde [nm]",ylab="Absorbance")
  #   mtext(nom_pretrait)
  #   }
  # } # fin boucle if (type_spectres == "NIRS")
  ################# fin version avec longueur d'onde colonne 324 supprimee
  
  #if (type_spectres == "NIRS") {
    
    assign(spec_trans_nom, 
           spec_trans[,c(-1,-ncol(spec_trans))], inherits=TRUE)
    
    wavelength <<- as.numeric(colnames(spec_trans[,c(-1,
                    -ncol(spec_trans))])) # on met aussi spectrans[,-1]
    # ici car pour NIRS spec_trans reste à 700 variables
    # et manifestement la commande suivante ne change pas spec_trans 
    # dans la fonction  assign(spec_trans_nom,   spec_trans[,-1], inherits=TRUE)
    # ci-dessous on met aussi t(spec_trans[,-1]) (puis aussi derniere colonne) 
    # pour voir  
    
    if(option_graph_pretrait =="TRUE") {
      matplot(wavelength,t(spec_trans[,c(-1,-ncol(spec_trans))]),
              type="l",col=c(1:12),lty=1,
              xlab="Longueur d'onde [nm]",ylab="Absorbance")
      mtext(nom_pretrait)
    }
  # } # fin boucle if (type_spectres == "NIRS")
  
 }

# Savitzky-Golay ----
# attention diminue le nb de variables
pretrait6 <- function(spec_init = Xbrut , spec_trans =Xm)
{
  spec_trans_nom <- deparse(substitute(spec_trans))
  # on transforme spec_trans en data.frame
  assign(spec_trans_nom, as.data.frame(savitzkyGolay(spec_init,m=1,p=4,w=5)) ,
         inherits=TRUE)
  # au lieu de wavelength[1:(dim(spec_trans)[2])] 
  # on utilise as.numeric(colnames(spec_trans))
  wavelength <<- as.numeric(colnames(spec_trans))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
      xlab="Longueur d'onde [nm]",ylab="1st derivative")
  mtext(nom_pretrait)
  }
}

# Gap-segment Derivative ordre 1 ---- 
# attention diminue le nb de variables
pretrait7 <- function(spec_init = Xbrut , spec_trans =Xm)
{
  
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom, as.data.frame(gapDer(spec_init,m=1,w=3,s=15)) , 
         inherits=TRUE)
  # w = 3 : filter length = spcing between point over which the derivative 
  #         is computed
  # s = 15 : segment size, the range over which the points are averaged 
  # (default = 1, ie no smoothing corresponding to 'Norris' Gap Derivative)
  wavelength <<- as.numeric(colnames(spec_trans))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="gap derivative")
  mtext(nom_pretrait) # avec fenêtre de 30 nm
  }
}

# Gap-segment Derivative ordre 2 ----
# attention  diminue le nb de variables
pretrait8 <- function(spec_init = Xbrut , spec_trans =Xm)
{
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom,as.data.frame(gapDer(spec_init,m=2,w=3,s=15)) , 
         inherits=TRUE)
  # w = 3 : filter length = spacing between point over which the derivative 
  #         is computed
  # s = 15 : segment size, the range over which the points are averaged 
  # (default = 1, ie no smoothing corresponding to 'Norris' Gap Derivative)
  wavelength <<- as.numeric(colnames(spec_trans))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="gap derivative")
  mtext(nom_pretrait) #  avec lissage sur 30 nm
  }
}

# Multiplicative Scatter/Signal Correction ----
# Attention a l'utilisation de MSC car c'est une transformation 
# dataset dependante 
pretrait9 <- function(spec_init = Xbrut , spec_trans =Xm)
{
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom, as.data.frame(msc(as.matrix(spec_init))) , 
         inherits=TRUE)
  # on a du transformer en data frame pour eviter probleme par la suite
  wavelength <<- as.numeric(colnames(spec_trans))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="Absorbance")
  mtext(nom_pretrait)
  }
}

#~~~ 2.2 fonctions necessitant package baseline ----

# baseline avec methode irls par defaut ----
pretrait10 <- function(spec_init = Xbrut , spec_trans =Xm)
{
  baselineXbrut <- baseline(as.matrix(spec_init))
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom, as.data.frame(getCorrected(baselineXbrut)), 
         inherits=TRUE)
  # nb ci dessus on a du reconvertir Xm en dataframe pour 
  # pas de blocage en classif non supervisee
  rm(baselineXbrut)
  wavelength <<- as.numeric(colnames(spec_init))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="Absorbance")
  mtext(nom_pretrait)
  }
}

# baseline avec methode als ----
pretrait11 <- function(spec_init = Xbrut , spec_trans =Xm)
{
  baselineXbrut <- baseline(as.matrix(spec_init),method="als")
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom, as.data.frame(getCorrected(baselineXbrut)), 
          inherits=TRUE) 
  # nb ci dessus on a du reconvertir Xm en dataframe 
  # pour pas de blocage en classif non supervisee
  rm(baselineXbrut)
  #wavelength <<- wavelength_brut
  wavelength <<- as.numeric(colnames(spec_init))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="Absorbance")
  mtext(nom_pretrait)
  }
}

# baseline avec methode peakDetection ----
pretrait12 <- function(spec_init = Xbrut , spec_trans =Xm)
{
  baselineXbrut <- baseline(as.matrix(spec_init),method="peakDetection")
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom, as.data.frame(getCorrected(baselineXbrut)), 
       inherits=TRUE)
  # probleme : peakDetection enleve les dimnames de Xm 
  # correspondant a rownanes et colnames !! 
  # ==> un bug plus loin au niveau de creation de XR
  # donc on rajoute des dimnames a Xm ((ce sont les dimnames de Xbrut)
  rm(baselineXbrut)
  dimnames(spec_trans) <- dimnames(spec_init)     # pour remettre les dimnames
  assign(spec_trans_nom, spec_trans, inherits=TRUE) # re ecrit spectrans 
                                                  # avec inherits = TRUE 
                                                  # pour conservation des 
                                                # dimnames hors de la fonction
  wavelength <<- as.numeric(colnames(spec_init))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
        xlab="Longueur d'onde [nm]",ylab="Absorbance")
  mtext(nom_pretrait)
  }
}
# baseline avec methode lowpass ----
pretrait13 <- function(spec_init = Xbrut , spec_trans =Xm)
{
  baselineXbrut <- baseline(as.matrix(spec_init),method="lowpass")
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom, as.data.frame(getCorrected(baselineXbrut)), 
         inherits=TRUE)
  # ci-dessus on a reconverti getCorrected en data.frame 
  # pour pas de blocage plus loin
  rm(baselineXbrut)
  #wavelength <<- wavelength_brut
  wavelength <<- as.numeric(colnames(spec_init))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="Absorbance")
  mtext(nom_pretrait)
  }
}

# baseline avec methode medianWindow ----
pretrait14 <- function(spec_init = Xbrut , spec_trans =Xm)
{
  baselineXbrut <- baseline(as.matrix(spec_init),hwm=300,method="medianWindow")
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom, as.data.frame(getCorrected(baselineXbrut)), 
         inherits=TRUE)
  # ci-dessus on a reconverti getCorrected en data.frame pour pas de blocage 
  # plus loin
  rm(baselineXbrut)
  #wavelength <<- wavelength_brut
  wavelength <<- as.numeric(colnames(spec_init))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="Absorbance")
  mtext(nom_pretrait)
  }
}

# baseline avec methode modpolyfit ----
pretrait15 <- function(spec_init = Xbrut , spec_trans =Xm)
{
  baselineXbrut <- baseline(as.matrix(spec_init),method="modpolyfit")
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom, as.data.frame(getCorrected(baselineXbrut)), 
         inherits=TRUE)
  # ci-dessus on a reconverti getCorrected en data.frame 
  # pour pas de blocage plus loin
  rm(baselineXbrut)
  # probleme : modpolyfit enlevait les dimnames de Xm 
  # correspondant a rownanes et colnames !! 
  # ceci creait un bug plus loin au niveau de creation de XR
  dimnames(spec_trans) <- dimnames(spec_init)       # debogage
  assign(spec_trans_nom, spec_trans, inherits=TRUE) # re ecrit spetrans 
                                                    # avec inherits = TRUE 
                                                # pour conservation des 
                                                # dimnames hors de la fonction
  wavelength <<- as.numeric(colnames(spec_init))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="Absorbance")
  mtext(nom_pretrait)
  }
}

# baseline avec methode rfbaseline ----
pretrait16 <- function(spec_init = Xbrut , spec_trans =Xm)
{
  baselineXbrut <- baseline(as.matrix(spec_init),method="rfbaseline")
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom, as.data.frame(getCorrected(baselineXbrut)),
         inherits=TRUE)
  # ci-dessus on a du reconvertir en data.frame le getCorrected
    rm(baselineXbrut)
  #wavelength <<- wavelength_brut
  wavelength <<- as.numeric(colnames(spec_init))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="Absorbance")
  mtext(nom_pretrait)
  }
}

# baseline avec methode rollingBall ----
pretrait17 <- function(spec_init = Xbrut , spec_trans =Xm)
{
  baselineXbrut <- baseline(as.matrix(spec_init),wm=200,ws=200,
                            method="rollingBall")
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom, as.data.frame(getCorrected(baselineXbrut)), 
         inherits=TRUE)
  # ci-dessus on a du reconvertir en data.frame le getCorrected
  rm(baselineXbrut)
  #wavelength <<- wavelength_brut
  wavelength <<- as.numeric(colnames(spec_init))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="Absorbance")
  mtext(nom_pretrait)
  }
}

#~~~ 2.3 autres fonctions  ----

# Centrage sans reduction ----
# adaptation de function Centrage reduction pour Centrage sans reduction
centrage <- function (X)  #fonction derivee de StandardNormalvariate
{
  if (!class(X) %in% c("matrix", "data.frame")) 
    stop("X should be a matrix or data.frame")
  X <- sweep(X, 1, rowMeans(X, na.rm = T), "-")
  # X <- sweep(X, 1, apply(X, 1, sd, na.rm = T), "/")
  return(X)
}

pretrait18 <- function(spec_init = Xbrut , spec_trans =Xm) 
{
  spec_trans_nom <- deparse(substitute(spec_trans))
  assign(spec_trans_nom,  centrage(spec_init) , inherits=TRUE)
  # au lieu de wavelength[1:(dim(spec_trans)[2])] 
  # on utilise as.numeric(colnames(spec_trans))
  wavelength <<- as.numeric(colnames(spec_trans))
  if(option_graph_pretrait =="TRUE") {
  matplot(wavelength,t(spec_trans[,]),type="l",col=c(1:12),lty=1,
          xlab="Longueur d'onde [nm]",ylab="Absorbance")
  mtext(nom_pretrait)
  }
}  

